module.exports = class ArtefactController {

    constructor(mainApp){
        this.mainApp = mainApp;

        this.FileSaver = require('file-saver');
        this.RecordRTC = require('recordrtc');
    }

    fileOpen(filter, callBack){
        /**
         * {
         * multiple: false,
         * accept: []
         * }
         */
        if(this.mainApp !== undefined){

            const { dialog } = require('electron').remote

            let query = ({})
            let properties = ['openFile', 'treatPackageAsDirectory', 'showHiddenFiles'];

            if(filter.multiSelections){
                properties.push('multiSelections');
            }
            
            if(filter.accept != undefined){
                query.filtres = filter.accept;
            }

            query.properties = properties;

            callBack(dialog.showOpenDialog({ properties: ['openFile', 'multiSelections'] }))

        } else {
            const fileDialog = require('file-dialog');
               
            fileDialog(filter)
            .then(callBack)
        }
    }

    saveText(text, url){
        var blob = new Blob([text], {type: "text/plain;charset=utf-8"});
        this.FileSaver.saveAs(blob, url);        
    }

    saveFromUrl(url1, url2){
        this.FileSaver.saveAs(url1, url2);
    }

    saveFromBlob(blob, url){
        this.FileSaver.saveAs(blob, url);
    }

    saveFromCanvas(canvas, url){
        canvas.toBlob(function(blob) {
            this.FileSaver.saveAs(blob, url);
        });
    }

    saveFile(data, url){
        let blob = new Blob([data], url, {type: "text/plain;charset=utf-8"});
        this.FileSaver.saveAs(blob);
    }

    vibrate(options){
        if(window != undefined){
            window.navigator.vibrate(options);
        }
    }

    stopVibration(){
        if(window != undefined){
            window.navigator.vibrate(0);
        }
    }

    callNumber(number){
        window.location.href="tel://"+number;
    }

    getBattery(callBack){
        navigator.getBattery().then(callBack);
    }

    checkUserMedia(){
        return (navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
    }

    stopStream(){
        if(window.stream){
            window.getTracks().forEach( (e) => e.stop())
        }
    }

    /**
     * 
     * @param {
                audio: {
                    width: 5 || {min:0, max:0}
                    deviceId: {exact: audioSelect.value}
                },
                video: {
                    deviceId: {exact: videoSelect.value}
                }
            } constraints 
     * @param {*} callback 
     */
    getMedia(constraints, callback){
        this.stopStream();

        navigator.mediaDevices.getUserMedia(constraints).
        then(callback).catch(this.handleError);
    }

    getAudioSources(callBack){
        navigator.mediaDevices.enumerateDevices().then( (devices) => {
            let list = devices.filter( (e) => e.kind === 'audioinput');
            callBack(list);
        }).catch(this.handleError);
    }

    getVideoSources(callBack){
        navigator.mediaDevices.enumerateDevices().then( (devices) => {
            let list = devices.filter( (e) => e.kind === 'videoinput');
            callBack(list);
        });
    }

    handleError(error) {
        alert('Error: ', error);
    }

    getScreenshot(video, format, compression){
        let form = format? 'image/' + format : 'image/jpg';
        let compres = compression? compression : 0;

        const canvas = document.createElement('canvas');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext('2d').drawImage(video, 0, 0);

        return canvas.toDataURL(form, compres);
    }

    getVideoRecorder(){
        return RecordRTC(stream, {type: 'video', mimeType: 'video/mp4'});
    }

    getAdioRecorder(){
        return RecordRTC(stream, {type: 'audio', mimeType: 'audio/wav'});
    }

    startRecording(recorder){
        recorder.startRecording();
    }

    stopRecording(recorder, callBack){
        recorder.stopRecording( () => {
            callBack(recorder.getBlob());
        });
    }

}